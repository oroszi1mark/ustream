define('videoHandler', ['dataHandler', 'config'], function(dataHandler, config) {

	var Data = dataHandler,
		_videos = _favorites = [],
		_filters = {},
		_init = function(data) { _videos = data },

		api = {

			/** Video list methods **/

			/**
			 * Return video object based on ID
			 * @param {int} id Video ID
			 * @returns {object} Video object
			 */
			getVideoInfo: function(id) {
				return this.getFilteredVideos().find(function(item) { return item.id === id });
			},

			/**
			 * Return array of video objects queried from server
			 * @returns {Array}
			 */
			getVideos: function() {
				return _videos;
			},

			/**
			 * Return array of filtered video objects using preset filters
			 * @returns {object}
			 */
			getFilteredVideos: function() {
				return Data.filterBy(_videos, _filters);
			},

			/**
			 * Fetch new video list form server
			 */
			updateVideos: function() {
				var _fetchVids = function(data) { _videos = data; },
					_fetchError = function(data) { console.error(data); };

				Data.getFromApi(_fetchVids, _fetchError);
			},

			/**
			 * Sort videos by item key
			 * @param {string} key Item property to sort by
			 * @param {boolean} [reverse] False = ascending order, true = descending order
			 * @returns {object} Array of sorted videos
			 */
			sortVideos: function(key, reverse) {
				var sort_fn;

				// Handle special sorts
				switch (key) {

					// Sort location by country and city
					case 'location':
						sort_fn = function(a,b) {
							a = a.location;
							b = b.location;

							// Countries are the same, sort by cities
							if (a.country.toLowerCase() === b.country.toLowerCase()) {
								var x = a.city.toLowerCase(),
									y = b.city.toLowerCase();

								return x < y ? -1 : x > y ? 1 : 0;
							}

							// Sort by countries
							a = a.country.toLowerCase();
							b = b.country.toLowerCase();

							return a < b ? -1 : a > b ? 1 : 0;
						};
						return Data.sortBy(this.getFilteredVideos(), key, reverse, sort_fn);

					// Put live videos first by default
					case 'isLive':
						reverse = reverse || true;

					// Perform default sort and return
					default:
						return Data.sortBy(this.getFilteredVideos(), key, reverse, sort_fn);
				}
			},

			/** Favorited list methods **/


			/**
			 * Get list of favorited videos
			 * @returns {object}
			 */
			getFavs: function() {
				return Data.getFromLocal(config.localstorage_favorites_id);
			},

			/**
			 * Add a video to the favorited list. Videos can be added only once.
			 * @param {int|object} favs One or more video IDs as an array
			 * @returns {object} The actual list of favorited videos
			 */
			setFavs: function(favs) {

				// Fetch actual list
				var favlist = this.getFavs();

				// Return if no param provided
				if (!favs) return favlist;

				// Init if nothing has been stored so far
				if (favlist === false) favlist = [];

				// Param is array
				if (Array.isArray(favs)) {
					// Filter duplicates
					var new_vids = favs.filter(function(item) { return !favlist.contains(item) });

					// Add new vids
					favlist = favlist.concat(favs);
				}
				// Param is singular
				else {
					if (!favlist.includes(favs)) favlist.push(favs);
				}

				// Update favorite list in LocalStorage
				Data.setInLocal(config.localstorage_favorites_id, favlist);

				// Return actual list
				return favlist;
			},

			/**
			 * Remove video from favorited list
			 * @param {int|object} favs One or more video IDs as an array
			 * @returns {object} The actual list of favorited videos
			 */
			removeFavs: function(favs) {
				var favlist = this.getFavs();

				if (!favs) return favlist;

				if (typeof favs == 'number') favs = [favs];

				favlist = favlist.filter(function(item) {
					return !favs.includes(item);
				});

				Data.setInLocal(config.localstorage_favorites_id, favlist);

				return favlist;
			},

			/**
			 * Check if a video is favorited
			 * @param {int} id Video ID
			 * @returns {boolean} True if video is favorited
			 */
			checkIfFav: function(id) {
				var favlist = this.getFavs();

				if (!favlist || isNaN(id)) return false;

				return favlist.includes(id);
			},

			/** Filter methods **/

			/**
			 * Add filter to apply on video list. Filters are cumulative.
			 * @param {string} key Video item property to filter by
			 * @param {*} value Value to filter by. Several values can be added to one key. This should be sanitized user input.
			 * @returns {object} The current list of all filters to be applied
			 */
			addFilter: function(key, value) {
				if (_filters[key]) _filters[key].push(value);
				else _filters[key] = [value];

				return _filters;
			},

			/**
			 * Remove a filter
			 * @param {string} key Video item property to filter by
			 * @param {*} [value] Value to filter by. Several values can be added to one key. If none added, all key filters are removed.
			 * @returns {object} The current list of all filters to be applied
			 */
			removeFilter: function(key, value) {
				// No value provided, all key filters are removed
				if (!value) {
					delete _filters[key];
					return _filters;
				}

				// Remove values from key
				if (_filters[key]) {
					var i = _filters[key].indexOf(value);

					if (i > -1) _filters[key].splice(i, 1);
				}

				return _filters;
			}

		};

	// Get list of videos from server
	Data.getFromApi(_init, null);

	// Get list of favorited videos
	_favorites = api.getFavs();

	// Return methods
	return api;
});