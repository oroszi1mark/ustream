define('videoList', ['videoHandler', 'react', 'videoCard'], function(videoHandler, React, VideoCard) {
	return React.createClass({
		/**
		 * Generate each VideoCard by mapping this.state.videos
		 * @returns {*}
		 * @private
		 */
		_createVideoCards: function() {
			return markup = this.state.videos.map(function(vid) {
				return React.createElement(
					VideoCard,
					{
						key: vid.id,
						id: vid.id,
						isLive: vid.isLive,
						type: vid.type,
						viewers: vid.viewers,
						title: vid.title,
						description: vid.description,
						picture: vid.picture,
						location: vid.location,
						labels: vid.labels
					}
				);
			});

		},

		/**
		 * Get videos from API
		 * @returns {*|Array}
		 * @private
		 */
		_fetchVideos() {
			return videoHandler.getVideos();
		},

		getInitialState: function() {
			return {
				filters: [],
				sort: {
					prop: '',
					reverse: false
				},
				pollInterval: 0,
				videos: this._fetchVideos()
			};
		},
		render: function() {
			return React.createElement(
				'main',
				{className: 'video-list'},
				this._createVideoCards()
			)
		}
	});
});