// todo turn into factory
define('dropdown', ['react', 'videoHandler'], function(React, videoHandler) {

	// Button
	var Btn = React.createClass({
		_getLabel: function() {
			// todo make this dynamic
			return 'Filter by video type...';
		},
		render: function() {
			return React.createElement(
				'button',
				{
					type: 'button',
					className: 'btn btn-default dropdown-toggle',
					dataToggle: 'dropdown',
					ariaHaspopup: 'true',
					ariaExpanded: 'false'
				},
				React.createElement('span', {className: 'glyphicon glyphicon-filter'}),
				this._getLabel(),
				React.createElement('span', {className: 'caret'})
			);
		}
	});

	// Dropdown
	var Dropdown = React.createClass({
		_getMenuItems: function() {},
		render: function() {
			return React.createElement(
				'ul',
				{className: 'dropdown-menu'},
				this._getMenuItems()
			);
		}
	});

	// Button group
	return React.createClass({
		// todo make options dynamic
		_getFilterTypes: function() {
			return {
				none: {
					label: 'Don\'t filter',
					action: this._setNone
				},
				live: {
					label: 'Live channel only',
					action: this._setLive
				},
				offline: {
					label: 'Offline channel only',
					action: this._setOffline
				},
				video: {
					label: 'Video only',
					action: this._setVideo
				}
			};
		},
		_setNone: function() {
			videoHandler.removeFilter('type');
			videoHandler.removeFilter('isLive');
		},
		_setLive: function() {
			this._setNone();
			videoHandler.addFilter('isLive', true);
		},
		_setOffline: function() {
			this._setNone();
			videoHandler.addFilter('isLive', false);
		},
		_setVideo: function() {
			this._setNone();
			videoHandler.addFilter('type', 'recorded');
		},
		_changeFilter: function() {

		},
		getInitialState: function() {
			return {
				activeFilter: this._getFilterTypes().none
			}
		},
		render: function() {
			return React.createElement(
				'div',
				{
					className: 'btn-group',
					role: 'group'
				},
				React.createElement(Btn, { activeFilter: this.state.activeFilter }),
				React.createElement(Dropdown, {activeFilter: this.state.activeFilter, filters: this._getFilterTypes()})
			)
		}
	});
});