define('videoCard', ['react', 'videoHandler'], function(React, videoHandler) {

	// Thumbnail
	var VideoThumbnail = React.createClass({
		render: function() {
			return React.createElement('figure', {className: 'vid-thumb'},
				React.createElement('img', {src: this.props.imgSrc, alt: this.props.vidTitle}, null)
			);
		}
	});

	// Title
	var VideoTitle = React.createClass({
		render: function() {
			return React.createElement('h4', {className: 'vid-title'},
				React.createElement('a', {}, this.props.vidTitle)
			);
		}
	});

	// Description
	var VideoInfo = React.createClass({
		render: function() {
			return React.createElement('main', {className: 'vid-info'}, this.props.vidInfo);
		}
	});

	// Map
	var VideoMap = React.createClass({
		/**
		 * Return proper class for showing/hiding map
		 * @returns {string} classes
		 * @private
		 */
		_getOwnClasses: function() {
			return 'vid-map '+ (!this.props.showMap ? 'inact' : '');
		},

		/**
		 * Get static map img URL based on location
		 * @returns {*} URL
		 * @private
		 */
		_getImgSrc: function() {
			var loc = this.props.vidLocation,
				lat = loc.coordinates.latitude,
				lng = loc.coordinates.longitude,
				url = '//maps.googleapis.com/maps/api/staticmap?center=<<lat>>,<<lng>>&zoom=12&markers=color:red%7C<<lat>>,+<<lng>>&scale=2&size=250x250&maptype=roadmap&format=png&visual_refresh=true';

			if (this.props.showMap || this.state.imgAlreadyLoaded) {
				this.state.imgAlreadyLoaded = true;

				return url.split('<<lat>>').join(lat).split('<<lng>>').join(lng); // Firing up the regex engine would be expensive
			}
			else return '';
		},

		getInitialState: function() {
			return {
				imgAlreadyLoaded: false
			}
		},
		render: function() {
			var enc = encodeURIComponent,
				loc = this.props.vidLocation;

			return React.createElement('figure', {className: this._getOwnClasses()},
				React.createElement(
					'a',
					{
						href: '//www.google.com/maps/place/' + enc(loc.city) +',+' + enc(loc.country),
						target: '_blank'
					},
					React.createElement(
						'img',
						{
							src: this._getImgSrc(),
							className: 'img-responsive',
							alt: loc.city +', '+ loc.country
						},
						null
					)
				)
			);
		}
	});

	// Labels
	var VideoLabels = React.createClass({
		/**
		 * Generate label <a> elements
		 * @returns {Array}
		 * @private
		 */
		_createLabels: function() {
			return this.props.vidLabels.map(function(label) {
				React.createElement('a', {className: 'label label-default', label});
			});
		},
		render: function() {
			return React.createElement('div', {},
				this._createLabels()
			);
		}
	});

	// Location
	var VideoLocation = React.createClass({
		render: function() {
			var loc = this.props.vidLocation;
			return React.createElement('a', {className: 'vid-location', onClick: this.props._toggleMap},
				React.createElement('span', {className: 'glyphicon glyphicon-globe'}, null),
				loc.city +', '+ loc.country
			);
		}
	});

	// Views
	var VideoViews = React.createClass({
		/**
		 * Add thousands separators to number
		 * @param {int} num
		 * @returns {string} Formatted number as string
		 * @private
		 */
		_formatNumber: function(num) {
			// Convert to string and add thousands separators
			var num_arr = parseInt(num).toString().split('').reverse(),
				formatted = [];

			for (var i=0; i<num_arr.length; i+=3) {
				formatted.push(num_arr.slice(i, i+3).reverse().join(''));
			}

			return formatted.reverse().join(' ');
		},
		render: function() {
			return React.createElement('span', {className: 'vid-views'},
				React.createElement('span', {className: 'glyphicon glyphicon-eye-open'}, null),
				this._formatNumber(this.props.vidViews)
			);
		}
	});

	// Favorite button
	var VideoFavorite = React.createClass({
		getInitialstate: function() {
			return {
				isFav: videoHandler.checkIfFav(this.props.vidId)
			}
		},
		render: function() {
			return React.createElement(
				'button',
				{
					className: 'fave-btn',
					title: 'Favorite',
					onClick: this.props._toggleFav
				},
				React.createElement('span', {className: 'glyphicon glyphicon-heart'}, null)
			);
		}
	});

	// Play button
	var VideoPlay = React.createClass({
		render: function() {
			return React.createElement('button', {className: 'play-btn', title: 'Play'},
				React.createElement('span', {className: 'glyphicon glyphicon-play'}, null)
			);
		}
	});

	// Video buttons
	var VideoButtons = React.createClass({
		render: function() {
			return React.createElement('div', {className: 'vid-btns'},
				React.createElement(VideoFavorite, {vidId: this.props.vidId, _toggleFav: this.props._toggleFav}),
				React.createElement(VideoPlay)
			);
		}
	});

	// Meta
	var VideoMeta = React.createClass({
		render: function() {
			return React.createElement('footer', {className: 'vid-meta'},
				React.createElement(VideoLocation, {vidLocation: this.props.vidLocation, _toggleMap: this.props._toggleMap}),
				React.createElement('br'),
				React.createElement(VideoViews, {vidViews: this.props.vidViews}),
				React.createElement(VideoLabels, {vidLabels: this.props.vidLabels}),
				React.createElement(VideoButtons, {vidId: this.props.vidId, _toggleFav: this.props._toggleFav})
			);
		}
	});

	// Video card
	return React.createClass({
		/**
		 * Show/hide static map
		 * @private
		 */
		_toggleMap: function() {
			this.setState({showMap: !this.state.showMap});
		},

		/**
		 * Toggle favorite status of video
		 * @private
		 */
		_toggleFav: function() {
			if (videoHandler.checkIfFav(this.props.id)) {
				videoHandler.removeFavs(this.props.id);
				this.setState({isFav: false});
			}
			else {
				videoHandler.setFavs(this.props.id);
				this.setState({isFav: true});
			}
		},

		/**
		 * Generate VideoCard classes based on favorite status
		 * @returns {string}
		 * @private
		 */
		_getOwnClasses: function() {
			return 'col-md-3 video '+ (this.state.isFav ? 'liked' : '');
		},
		getInitialState: function(){
			return {
				isFav: videoHandler.checkIfFav(this.props.id),
				showMap: false
			};
		},
		render: function() {
			return React.createElement(
				'article', {className: this._getOwnClasses()},
				React.createElement(VideoThumbnail, {imgSrc: this.props.picture, vidTitle: this.props.title}),
				React.createElement(VideoTitle, {vidTitle: this.props.title}),
				React.createElement(VideoInfo, {vidInfo: this.props.description}),
				React.createElement(VideoMap, {vidLocation: this.props.location, showMap: this.state.showMap}),
				React.createElement(VideoMeta, {
					vidId: this.props.id,
					vidLocation: this.props.location,
					vidLabels: this.props.labels,
					vidViews: this.props.viewers,
					_toggleMap: this._toggleMap,
					_toggleFav: this._toggleFav
				})
			);
		}
	});
});