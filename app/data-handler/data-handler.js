define('dataHandler', ['jQuery', 'config'], function($, config) {

	return {
		/**
		 * Query video list from server, then execute callback function
		 * @param {function} [success_callback] Gets executed on successful AJAX call
		 * @param {function} [error_callback] Gets executed on failed AJAX call
		 */
		getFromApi: function(success_callback, error_callback) {
			jQuery.ajax({
				url: config.api_url,
				dataType: 'jsonp',
				success: function(data) { success_callback(data) },
				error: function(data) { error_callback(data) }
			});
		},

		/**
		 * Get JSON object key's value from LocalStorage
		 * @param {string} key JSON object key
		 * @returns {object} JSON object of current LocalStorage data
		 */
		getFromLocal: function(key) {
			var local = JSON.parse(window.localStorage.getItem(config.localstorage_id));
			return local ? local[key] : false;
		},

		/**
		 * Set key-value pair in LocalStorage
		 * @param {string} key JSON object key
		 * @param {*} val JSON object value
		 * @returns {object} Returns LocalStorage date inside object with status information
		 */
		setInLocal: function(key, val) {
			var storage = window.localStorage.getItem(config.localstorage_id);

			// Init storage if it is null
			if (storage === null) storage = {};

			// Parse existing data
			else storage = JSON.parse(storage);

			// Add new data
			storage[key] = val;

			// Store updated object
			try {
				window.localStorage.setItem(config.localstorage_id, JSON.stringify(storage));
				return {status: 'ok', data: storage};
			}
			catch (e) {
				return {status: 'error', error: e, data: JSON.parse(window.localStorage.getItem(config.localstorage_id))};
			}
		},

		/**
		 * Sort an array of objects by key
		 * @param {object} data Array of objects
		 * @param {string} key Object key the sorting is based on
		 * @param {boolean} [reverse] False = ascending order, true = descending order
		 * @param {function} [sort_fn] Custom sorting function that's valid for Array.prototype.sort()
		 * @returns {object} Sorted array
		 */
		sortBy: function(data, key, reverse, sort_fn) {

			// Accept valid non-empty arrays
			if (!Array.isArray(data) || data.length === 0) return false;

			// Set default key
			key = key || 'id';

			// Set default sort function
			sort_fn = sort_fn || function(a,b) {

				// Sort numbers
				if (typeof a[key] == 'number' && typeof b[key] == 'number') return a[key] - b[key];

				// Sort booleans
				if (typeof a[key] == 'boolean' && typeof b[key] == 'boolean') return Number(a[key]) - Number(b[key]);

				// Sort strings
				var x = a[key].toLowerCase(),
					y = b[key].toLowerCase();
				return x < y ? -1 : x > y ? 1 : 0;
			};

			// Return sorted array
			return reverse ? data.sort(sort_fn).reverse() : data.sort(sort_fn);
		},

		/**
		 * Filter array of data using a filter object
		 * @param {object} data Array of data containing objects
		 * @param {object} filters Filter where keys are item properties and values are arrays of values to filter for
		 * @param {boolean} [invert] If true, elements matching filter criteria will be EXCLUDED
		 * @returns {object} Filtered array
		 */
		filterBy: function(data, filters, invert) {
			if (!data) return false;
			if (!filters) return data;

			var filtered = data;

			// Loop through property keys
			for (var key in filters) {

				if (filters.hasOwnProperty(key)) {
					var filterkey = filters[key],
						val;

					// Loop through key values to filter for
					while (val = filterkey.pop()) {

						// Convert boolean strings to booleans
						if (val == "true") val = true;
						if (val == "false") val = false;

						// Perform filtering
						filtered = filtered.filter(function(item) {

							// Invert result if necessary
							return invert ? (item[key] !== val) : (item[key] === val);
						});
					}
				}
			}

			// Return
			return filtered;
		}
	}
});