
requirejs.config({
	baseUrl: './'
});

describe('Data-handler class', function() {
	var DataHandler = require('./data-handler'),
		data_handler;

	beforeEach(function() {
		// http://stackoverflow.com/a/15464313
		require(['data-handler'], function(_datahandler) {
			data_handler = new _datahandler;
			done();
		});
	});

	it('should have jQuery', function() {
		expect(typeof jQuery).not.toBe(undefined);
	});

	it('should be instanceof class DataHandler', function() {
		expect(data_handler).toBe(DataHandler);
	});
});