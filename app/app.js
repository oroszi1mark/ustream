requirejs.config({
	paths: {
		// Plugins
		jQuery: "../node_modules/jquery/dist/jquery.min",
		react: "../node_modules/react/dist/react",
		reactDom: "../node_modules/react-dom/dist/react-dom",

		// Business logic
		config: "./config",
		dataHandler: "./data-handler/data-handler",
		videoHandler: "./video-handler/video-handler",

		// React components
		videoCard: "./components/video-card/video-card",
		videoList: "./components/video-list/video-list"
	}
});

requirejs(	['dataHandler', 'videoHandler', 'react', 'reactDom', 'videoList'],
	function(dataHandler, videoHandler, React, ReactDOM, VideoList) {

	// Launch React
	// todo: replace with event listener
	setTimeout(function(){ ReactDOM.render(React.createElement(VideoList), document.getElementById('root')) }, 1500);
});