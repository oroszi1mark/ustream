# Installation
Run `npm install`

# Structure
* `app` - business logic and React elements
    * `components` - React elements
        * `toolbar/dropdown.js` - Factory for generating Bootstrap dropdown elements (unfinished)
        * `video-card/video-card.js` - React element for each video
        * `video-list/video-list.js` - React element for generating list of videos
    * `data-handler` - Logic for fetching data from the server and handling LocalStorage
    * `video-handler` - Logic for listing, sorting, filtering, favoriting, updating videos
    * `app.js` - RequireJS config and React initialization
    * `config.js` - RequireJS AMD for configuring API URL
* `dist` - front-end resources
* `node_modules` - 3rd party packages
* `spec/support/jasmine.json` - Jasmine configuration

# Completed
* Get list of videos from server
* Generate list of videos
* Enable user to mark videos as favorites
* Store favorites in LocalStorage
* Sorting and filtering logic ready for use

# Todo
* Implement sorting and filtering by creating React UI elements
* Implement configurable server polling interval
* Use event emission instead of setTimeout when launching React app
* Create section for list of favorited videos
* Generate list of favorited videos and check by ID if they are still available
* Write unit tests for each component
* Figure out how to pass parent props to nth child component directly

# Difficulties
* Figuring out a way to use Babel, Jasmine and RequireJS together
* Forcing Babel to ignore spec files
* Figuring out how to extend ES6 classes from separate files loaded via RequireJS
* Figuring out how to add Bootstrap-specific attributes to React UI elements (e.g. dropdown buttons) using ES5